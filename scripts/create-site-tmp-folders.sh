#!/bin/bash

if [ ! -f .root ]
then
  echo "==> ERROR: run script from the web root"
  exit
fi

if [ ! -d web/sites ]
then
  mkdir -p web/sites
fi

for fn in `find web/sites -maxdepth 2 -type f -name 'settings.local.php' -exec dirname {} \; | sort | grep -o '[^\/]*$'`
do 
  if [ ! -d web/sites/$fn/tmp ]
  then 
    mkdir -p web/sites/$fn/tmp
    cp scripts/sites-tmp-htaccess web/sites/$fn/tmp/
  fi
done
