#!/bin/bash

umask 0002

if [ ! -f .root ]
then
  echo "==> ERROR: run script from the web root"
  exit
fi

echo "
==> Processing ownership"
find . -not \( -user tcas-drupal \) -exec chown tcas-drupal {} \;

echo "
==> Processing core files"
find . -not -path "./web/sites/*" -exec chmod g+w {} \;

cd web/sites
echo "
==> Processing sites (exclude files)"
find . -not \( -perm g+w -and -name 'settings*.php' -and -name 'services.yml'-and -path './*/files/*' \) -exec chmod g+w {} \;

echo "
==> Processing readonly files"
find ./* -maxdepth 1 -name 'services.yml' -exec chmod 0440 {} \;
find ./* -maxdepth 1 -name 'settings*.php' -exec chmod 0440 {} \;
find .  -maxdepth 1 -type d -exec chmod g-w {} \;

echo "
==> Processing files/ folders"
find ./*/files/ -exec chmod g+w {} \;

echo "
==> Processing misc files"
chmod 640 auth.json

