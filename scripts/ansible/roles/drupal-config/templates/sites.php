<?php
#
# Warning, this file is managed by Ansible. Any changes will be overwritten
#
$sites = array(
//'site-test.trinity.duke.edu' => 'site.trinity.duke.edu',
{% if sites is defined %}
{% for item in sites %}
  '{{ item.sites_servername }}' => '{{ item.sites_sitename }}',
{% endfor %}
{% endif %}
);
