#!/bin/bash

drupalroot="{{drupalroot}}"
sitesroot="{{drupalroot}}/web/sites"

function git_pull {
  # git clone <url> --branch <branch> --single-branch [<folder>]
  git clone https://at:{{token}}@gitlab.oit.duke.edu/$2/$3.git --branch $4 --single-branch $1
}

if [ ! -d $sitesroot ]
then
  echo "==> Sites folder does not exist: $sitesroot"
  exit
fi

cd $sitesroot

umask 0002

{% for item in vhosts.values() %}
git_pull {{ item.drupalsitename }} {{ item.gitproject }} {{ item.gitsitename }} {{ item.gitbranch }}
{% endfor %}
