#!/bin/bash

mydir=`pwd`
if [ ! -f .root ]
then
  echo "==> ERROR: run script from the web root"
  exit
fi

for fn in `find web/sites -maxdepth 2 -type f -name 'settings.local.php' -exec dirname {} \; | sort | grep -o '[^\/]*$'`
do 
  if [ ! -d private/$fn ]
  then 
    rsync -av private/default/ private/$fn/
  fi
done
