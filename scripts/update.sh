#!/bin/bash

if [ $# -ne 1 ]
then
  echo "ERROR, please pass the drupalsitename or themename as a runtime arguements"	
  exit
fi
drupalsitename=$1

date_time=`date +"%Y%m%d-%HH%MM"`
echo "
==> Start Time: $date_time"

u=`whoami`
echo "
==> User: $u"

echo "
==> Beginning Update for: $drupalsitename"

drupalroot="/var/www/html"
scratchspace="/var/local/scratch"

function update_site {
  sitename="$drupalroot/web/sites/$drupalsitename"
  if [ ! -d $sitename ]
  then
    echo "Error, site does not exist: $sitename"
    exit
  fi
  drush_f="$drupalroot/vendor/bin/drush"
  
  echo "
==> CD'ing to $sitename"
  cd $sitename

  # preprocessing
  #$drush_f sset system.maintenance_mode 1
  #$drush_f sql-dump > "$scratchspace/$drupalsitename-$date_time.bak.sql"
  #ls -ltr $scratchspace | tail -1
  #echo "DB Backup: $scratchspace/$drupalsitename-$date_time.bak.sql"
  
  # git pull
  branch=`git branch | grep \* | cut -d ' ' -f2`
  
  echo "
==> Performing git reset 'hard' on $branch to origin/$branch"
  git fetch --all
  git reset --hard origin/$branch
  
  # post processing
  echo "
==> Performing post-processing tasks (drush cr, drush cim)"
  $drush_f cr
  $drush_f cim -y
  #$drush_f sset system.maintenance_mode 0
}

function update_theme {
  echo "
==> Performing a 'composer update tcas/base-theme'"
  cd $drupalroot
  ./scripts/composer update tcas/tts_base

#  echo "
#==> Performing a git reset 'hard' on $branch to origin/$branch"
#  cd $drupalroot/web/themes/custom/tts_base
#  git fetch --all
#  git reset --hard origin/$branch
}

function report_out {
  # report out
  date_time=`date +"%Y%m%d-%HH%MM"`
  echo "
  ==>   EndTime: $date_time"
}

mydir=`pwd`
case $drupalsitename in
'tts-base') # git@gitlab.oit.duke.edu:tts-drupal-sites/drupal8-base.git
  update_theme
  ;;
*)
  update_site $drupalsitename
  ;;
esac
report_out
