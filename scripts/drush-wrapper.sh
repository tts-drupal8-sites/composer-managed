#!/bin/bash

umask 0002

if [ $# -eq 2 ]
then
  drupalroot="$2"
else
  drupalroot="/var/www/html"
fi

dt=`/bin/date +"%Y-%m-%d.%H.%M.%S"`
/bin/echo "Timestamp: $dt"

drush_cmd="$drupalroot/vendor/bin/drush"
sitesroot="$drupalroot/web/sites"
error_msg=''
printf_f=`which printf`
non_shib_users=0
editor_emails=0
editor_rpt=''
rpt="[select uid, name, mail, status from users where name not like '%@%' and name not like 'admin' and not uid='0';]\n"

function run_command {
  /bin/echo -e "\n==> Running $2 on $1"

  cd $sitesroot/$1
  
  if [ "$2" == "migrate-import-department-members" ]
  then
    $drush_cmd -l $1  migrate:import department_members --sync -y 2>&1
  else
    if [ "$2" == "non-shib-users" ]
    then
      rslt=`$drush_cmd -l $1 sql-query "select uid, name, mail, status from users where name not like '%@%' and name not like 'admin' and not uid='0';"`
      if [ ! "$rslt" == "" ]
      then
        rpt="$rpt\n\n===> Usernames not containing '@': $1\n"
        rpt="$rpt\n$rslt"
        non_shib_users=1
      fi
    else
      if [ "$2" == "editor-emails" ]
      then
        rslt=`$drush_cmd uinf --field=mail --uid=$($drush_cmd sqlq "SELECT GROUP_CONCAT(entity_id) FROM user__roles WHERE roles_target_id = 'site_editor' OR roles_target_id = 'content_author'")`
        if [ ! "$rslt" == "" ]
        then
          echo -e "$rslt"
          editor_rpt="$editor_rpt\n$rslt"
          editor_emails=1
        fi
      else
        if [ "$2" == "trinity-news" ]
        then
          $drush_cmd migrate:import trinity_news --sync -y 2>&1
        else
          $drush_cmd -l $1 $2 -y 2>&1
        fi
      fi
    fi
  fi
}

function validate_command {
  command=$1
  #/bin/echo "command: $command
#"
  pattern=".*($command).*"
  rslt=`/bin/echo "$command" | grep -o 'cc all\|config-export -y\|config-import -y\|cron\|cr\|entup -y\|editor-emails\|flush-image-cache\|migrate-import-department-members\|rr\|updb -y\|sset system.maintenance_mode 0\|sset system.maintenance_mode 1\|status\|trinity-news\|user-block 1\|\|user-unblock 1\|user:block admin,tts-admin\|user:unblock admin,tts-admin\|non-shib-users\|vset site_readonly 0\|vset site_readonly 1'`
  if [  ! "$command" == "$rslt" ]
  then
    msg="==> drush '$command' - not allowed
"
    /bin/echo "$msg"
    exit 1
  fi 
  #read -p "..."
}

if [ $# -lt 1 ] 
then 
  /bin/echo "==> Error: expecting quoted drush command: ex. 'cc all'"
  echo "
Valid Options:
==============
'cc all'
'config-export -y'
'config-import -y'
'cron'
'cr'
'entup -y'
'editor-emails'
'flush-image-cache'
'migrate-import-department-members'
'rr'
'updb -y'
'sset system.maintenance_mode 0'
'sset system.maintenance_mode 1'
'status'
'trinity-news'
'user-block 1'
'user-unblock 1'
'user:block admin,tts-admin'
'user:unblock admin,tts-admin'
'non-shib-users'
'vset site_readonly 0'
'vset site_readonly 1'
"

  exit 1
fi
command="$1"
validate_command "$command"

os=`uname -a | grep -o el7`
#if [ "$os" == "el7" ]
#then
#  source /opt/rh/rh-php73/enable;
#fi

for drupal_site in `cd $sitesroot;find -maxdepth 2 -type f -name "settings.local.php" -exec dirname {} \; | sed -e 's/^\.\///g' | sort`
do
  $printf_f "\n==> Proccessing $drupal_site"
  run_command $drupal_site "$command"
  #read -p "..."
done

if [ $non_shib_users -eq 1 ]
then
  /bin/echo -e "$rpt"
fi

if [ $editor_emails -eq 1 ]
then
  echo "
==> Editor Emails List"
  /bin/echo -e "$editor_rpt" | sort | uniq
fi

end_dt=`/bin/date +"%Y-%m-%d.%H.%M.%S"`

echo "
Script Started at: $dt
Script   Ended at: $end_dt
"
